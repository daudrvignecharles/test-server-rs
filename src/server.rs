extern crate iron;
extern crate router;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

use iron::prelude::*;
use iron::status;
use router::Router;
use std::error::Error;

use std::net::IpAddr;

struct Server {
    address: (IpAddr, u16),
}

impl Server {
    pub fn new(ip: &str, port: &str) -> Result<Self, String> {
        let ip: std::net::IpAddr = match ip.parse() {
            Ok(ip)  => ip,
            Err(e)  => return Err(e.description().to_owned()),
        };
        let port: u16 = match port.parse() {
            Ok(p)   => p,
            Err(e)  => return Err(e.description().to_owned()),
        };

        Ok(Server {
            address: (ip, port),
        })
    }

    pub fn launch(&mut self) {
        let mut router = Router::new();

        router.get("/", Self::handler, "root");
        router.get("/:action", Self::handler, "action");
        router.post("/connect", Self::client_connect, "connect");

        Iron::new(router).http(self.address).unwrap();
    }

    fn client_connect(request: &mut Request) -> IronResult<Response> {
        Ok(Response::with((status::Ok, "Connected")))
    }

    fn handler(request: &mut Request) -> IronResult<Response> {
        match request.extensions.get::<Router>() {
            None => Ok(Response::with((status::NoContent, "No content, error"))),
            Some(query) => Self::send_message(query)
        }
    }

    fn send_message(router_params: &router::Params) -> IronResult<Response> {
        let content = if let Some(argument) = router_params.find("action") {
            if argument == "connect" {
                use std::io::Read;
                let mut html_page = String::new();
                let mut f = std::fs::File::open("src/connect.html").expect("Unable to open connect.html");
                f.read_to_string(&mut html_page).expect("Unable to read string");
                let content_type = "text/html".parse::<iron::mime::Mime>().unwrap();
                //Ok(Response::with((content_type, status::Ok, INDEX_HTML)))
                return Ok(Response::with((content_type, status::Ok, html_page)));
            } else {
                format!("You are currently using action with argument `{}`", argument)
            }
        } else {
            "It works, guys!".to_owned()
        };
        let message = ChatMessage::new("server".to_owned(), content);
        let payload = serde_json::to_string(&message).unwrap();
        Ok(Response::with((status::Ok, payload)))
    }
}

fn main() {
    let mut args = std::env::args().skip(1).fuse();

    if let (Some(ip), Some(port)) = (args.next(), args.next()) {
        match Server::new(&ip, &port) {
            Ok(mut server)  => server.launch(),
            Err(e)          => println!("Error: {}.", e),
        }
    } else {
        usage()
    };

    println!("Server ended");

    fn usage() -> ! {
        println!("Usage: ./server ip_address port");
        std::process::exit(-1)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ChatMessage {
    name: String,
    message: String,
}

impl ChatMessage {
    pub fn new(name: String, message: String) -> Self {
        ChatMessage {
            name: name,
            message: message,
        }
    }
}
