# How to run the project?

- Install `rustup`:

```
$ curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain nightly
```

- Normally, `rustc` and `cargo` are automaticaly installed. Verify:
```
$ rustc --version
```
    
If `rustc` is not found:

```
$ rustup update && rustup install rustc cargo
```

- Go in the project root and run:

```
$ cargo run --bin server 127.0.0.1 3000
```

This may take a while the first time, the tool needs to download dependencies.
    
# How does it work?

When you run it, you need two parameters: the address and the port.

If you want to test, you can use the browser, typing `http://localhost:3000/` or you can use `curl`:

```
$ curl http://localhost:3000/
```